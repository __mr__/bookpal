from django.urls import path, include

from . import views

app_name="account"

urlpatterns = [
        path('login/', views.user_login, name = 'login'),
        path('logout/', views.user_logout, name = 'logout'),
        path('', views.dashboard, name = 'dashboard'),
        path('register/', views.register, name='register'),
        path('users/', views.users_list, name="users_list"),
        path('users/<username>/', views.user_info, name="user_info"),
        path('edit_profile/', views.edit_profile, name="profile_edit")
]
