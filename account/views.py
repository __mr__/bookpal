from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.urls import reverse

from .forms import LoginForm, UserRegistrationForm, UserEditForm
from library.models import BookProfile, UserProfile, Book

# Create your views here.
@login_required
def dashboard(req):
    profile = UserProfile.objects.get(profile_user=req.user)
    books_borrowed = profile.profile_books_borrowed.all()

    if req.method == "POST":
        book_to_remove_slug = req.POST['pk']
        book_to_remove = get_object_or_404(Book, book_slug = book_to_remove_slug)
        book_to_remove_profile = BookProfile.objects.get(bookprofile_user=req.user.user_profile, bookprofile_book=book_to_remove)
        book_to_remove_profile.delete()
        return HttpResponseRedirect(reverse("account:dashboard"))

    return render(req, 'account/dashboard.html', { 
                'profile':profile,
                'books': books_borrowed,
                })

@login_required
def edit_profile(req):
    user = req.user
    profile = UserProfile.objects.get(profile_user=user)


    data = {
            'first_name':req.user.first_name,
            'last_name':req.user.last_name,
            'email':req.user.email,
            'status':req.user.user_profile.profile_status,
    }

    if req.method == "POST":
        edit_form = UserEditForm(data=req.POST)
        if edit_form.is_valid():
            cd = edit_form.cleaned_data
            user.first_name = cd['first_name']
            user.last_name = cd['last_name']
            user.email = cd['email']
            profile.profile_status = cd['status']

            profile.save()
            user.save()

            return HttpResponseRedirect(reverse("account:dashboard"))
    else:
        edit_form = UserEditForm(data=data)

    return render(req, 'account/user_edit.html', { 'edit_form':edit_form })


def register(req):
    if req.method == "POST":
        user_form = UserRegistrationForm(req.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()

            profile = UserProfile.objects.create(profile_user=new_user, profile_status="online")
            new_user.user_profile = profile
            new_user.save()

            login(req, new_user)
            return HttpResponse("Your account has been registered!!")
    else:
        user_form = UserRegistrationForm()
    return render(req, 'account/register.html', {'user_form':user_form})

def user_login(req):
    if req.method == "POST":
        next = req.POST.get('next', '/')
        form = LoginForm(req.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(req, username = cd['username'], password = cd['password'])
            
            if user is not None:
                if user.is_active:
                    login(req, user)
                    if next:
                        return HttpResponseRedirect(next)
                    else:
                        return HttpResponseRedirect(reverse("account:dashboard"))
                else:
                        return HttpResponse("Disabled account!!")
        else:
            return HttpResponse("Invalid login!!")
    else:
        form = LoginForm()

    return render(req, 'account/login.html', {'form':form})

def user_logout(req):
    logout(req)
    return HttpResponseRedirect(reverse('library:index'))

@login_required
def users_list(req):
    users = UserProfile.objects.all()
    return render(req, 'account/users.html', {'users':users})

@login_required
def user_info(req, username):
    user = get_object_or_404(User, username=username)
    profile = UserProfile.objects.get(profile_user=user)

    return render(req, 'account/user_info.html', {'profile':profile})