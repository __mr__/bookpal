-- MySQL dump 10.17  Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: mr_library
-- ------------------------------------------------------
-- Server version	10.3.22-MariaDB-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add author',1,'add_author'),(2,'Can change author',1,'change_author'),(3,'Can delete author',1,'delete_author'),(4,'Can view author',1,'view_author'),(5,'Can add book',2,'add_book'),(6,'Can change book',2,'change_book'),(7,'Can delete book',2,'delete_book'),(8,'Can view book',2,'view_book'),(9,'Can add user profile',3,'add_userprofile'),(10,'Can change user profile',3,'change_userprofile'),(11,'Can delete user profile',3,'delete_userprofile'),(12,'Can view user profile',3,'view_userprofile'),(13,'Can add log entry',4,'add_logentry'),(14,'Can change log entry',4,'change_logentry'),(15,'Can delete log entry',4,'delete_logentry'),(16,'Can view log entry',4,'view_logentry'),(17,'Can add permission',5,'add_permission'),(18,'Can change permission',5,'change_permission'),(19,'Can delete permission',5,'delete_permission'),(20,'Can view permission',5,'view_permission'),(21,'Can add group',6,'add_group'),(22,'Can change group',6,'change_group'),(23,'Can delete group',6,'delete_group'),(24,'Can view group',6,'view_group'),(25,'Can add user',7,'add_user'),(26,'Can change user',7,'change_user'),(27,'Can delete user',7,'delete_user'),(28,'Can view user',7,'view_user'),(29,'Can add content type',8,'add_contenttype'),(30,'Can change content type',8,'change_contenttype'),(31,'Can delete content type',8,'delete_contenttype'),(32,'Can view content type',8,'view_contenttype'),(33,'Can add session',9,'add_session'),(34,'Can change session',9,'change_session'),(35,'Can delete session',9,'delete_session'),(36,'Can view session',9,'view_session'),(37,'Can add book profile',10,'add_bookprofile'),(38,'Can change book profile',10,'change_bookprofile'),(39,'Can delete book profile',10,'delete_bookprofile'),(40,'Can view book profile',10,'view_bookprofile');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$180000$11Zr5ALizB4a$AnQNV+3XeyrXnANJlxRzk8YWgAXd5N5ZD8xRCwvjOa4=','2020-09-15 10:24:06.176259',1,'manager','ma','a','manager@bookpal.com',1,1,'2020-09-01 11:55:31.741153'),(3,'pbkdf2_sha256$180000$13akfF5zZKPt$ia5yq6ODnOa0SLrhZzjTWxrOrlg2zUFYh14PC8IDOSE=','2020-09-15 11:29:27.829614',0,'a_user','a','user','a@user.com',0,1,'2020-09-02 11:20:34.436553'),(4,'pbkdf2_sha256$180000$mV45TbARUEFh$79HmYf3pZcJ2Iceyk1D/45THsPaCp0eY/V7MaZYg0VU=','2020-09-15 08:31:10.261195',0,'sogand','khosravi','','sogand@gmail.com',0,1,'2020-09-02 12:10:05.646919'),(5,'pbkdf2_sha256$180000$vNzdwTvcuMx7$9b21DybGKktM4UtdQCjCLI+Br1ItY3bxXkq8u+QinCQ=','2020-09-15 08:30:57.280458',0,'b_user','b','user','b@user.com',0,1,'2020-09-03 09:05:36.176004');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2020-09-03 06:23:30.734949','1','Rick & Morty',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(2,'2020-09-03 06:27:36.555750','2','Harry Potter and the Order of the Phoenix',1,'[{\"added\": {}}]',2,1),(3,'2020-09-03 06:34:13.894956','2','J.K. Rowling',1,'[{\"added\": {}}]',1,1),(4,'2020-09-03 06:35:11.951975','3','Stephenie Meyer',1,'[{\"added\": {}}]',1,1),(5,'2020-09-03 06:36:44.140855','3','Twilight',1,'[{\"added\": {}}]',2,1),(6,'2020-09-03 06:36:50.706842','2','Harry Potter and the Order of the Phoenix',2,'[{\"changed\": {\"fields\": [\"Book author\"]}}]',2,1),(7,'2020-09-03 06:38:36.451381','4','Harry Potter and the Cursed Child',1,'[{\"added\": {}}]',2,1),(8,'2020-09-03 06:46:40.540037','5','Harry Potter and the Half-Blood Prince',1,'[{\"added\": {}}]',2,1),(9,'2020-09-04 04:09:03.134549','2','a_user',2,'[{\"changed\": {\"fields\": [\"Profile books borrowed\"]}}]',3,1),(10,'2020-09-05 07:34:49.394209','4','Rick & Morty borrowed by:b_user, until 2020-09-19 12:04:47+04:30',1,'[{\"added\": {}}]',10,1),(11,'2020-09-06 05:44:17.022887','6','Memoirs of a Geisha',1,'[{\"added\": {}}]',2,1),(12,'2020-09-06 05:45:08.449137','4','Arthur Golden',1,'[{\"added\": {}}]',1,1),(13,'2020-09-06 05:45:23.869964','6','Memoirs of a Geisha',2,'[{\"changed\": {\"fields\": [\"Book author\"]}}]',2,1),(14,'2020-09-06 05:46:28.085950','5','Margaret Mitchell',1,'[{\"added\": {}}]',1,1),(15,'2020-09-06 05:48:23.812226','7','Gone with the Wind',1,'[{\"added\": {}}]',2,1),(16,'2020-09-06 05:52:18.340815','6','George Orwell',1,'[{\"added\": {}}]',1,1),(17,'2020-09-06 05:53:48.977692','8','1984',1,'[{\"added\": {}}]',2,1),(18,'2020-09-06 06:30:52.077507','2','Harry Potter and the Order of the Phoenix',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(19,'2020-09-06 06:30:57.317150','3','Twilight',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(20,'2020-09-06 06:31:05.459617','4','Harry Potter and the Cursed Child',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(21,'2020-09-06 06:31:11.601200','5','Harry Potter and the Half-Blood Prince',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(22,'2020-09-06 06:31:17.034148','6','Memoirs of a Geisha',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(23,'2020-09-06 06:31:23.051215','7','Gone with the Wind',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(24,'2020-09-06 06:38:30.380248','8','1984',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(25,'2020-09-06 06:38:39.080551','1','Rick & Morty',2,'[{\"changed\": {\"fields\": [\"Book genre\"]}}]',2,1),(26,'2020-09-06 07:55:30.212001','7','Twilight borrowed by:a_user, until 2020-09-06 19:30:00+00:00',3,'',10,1),(27,'2020-09-06 08:00:14.242938','4','Rick & Morty borrowed by:b_user, until 2020-09-19 07:34:47+00:00',3,'',10,1),(28,'2020-09-06 08:35:21.825875','8','1984 borrowed by:a_user, until 2020-09-13 19:30:00+00:00',3,'',10,1),(29,'2020-09-06 08:35:21.942736','6','Gone with the Wind borrowed by:a_user, until 2020-09-12 19:30:00+00:00',3,'',10,1),(30,'2020-09-06 08:35:21.979899','5','Rick & Morty borrowed by:a_user, until 2020-09-12 19:30:00+00:00',3,'',10,1),(31,'2020-09-14 10:08:41.641033','11','Rick & Morty borrowed by:manager, until 2020-09-14 19:30:00+00:00',3,'',10,1),(32,'2020-09-14 10:08:41.976297','10','Rick & Morty borrowed by:manager, until 2020-09-14 19:30:00+00:00',3,'',10,1),(33,'2020-09-15 08:19:11.243465','4','b_user',2,'[{\"changed\": {\"fields\": [\"Profile status\"]}}]',3,1),(34,'2020-09-15 08:19:19.244367','3','sogand',2,'[{\"changed\": {\"fields\": [\"Profile status\"]}}]',3,1),(35,'2020-09-15 08:19:24.878123','2','a_user',2,'[{\"changed\": {\"fields\": [\"Profile status\"]}}]',3,1),(36,'2020-09-15 08:33:46.611340','1','manager',2,'[{\"changed\": {\"fields\": [\"Profile status\"]}}]',3,1),(37,'2020-09-15 10:10:16.707569','2','a_user',2,'[{\"changed\": {\"fields\": [\"Profile status\"]}}]',3,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (4,'admin','logentry'),(6,'auth','group'),(5,'auth','permission'),(7,'auth','user'),(8,'contenttypes','contenttype'),(1,'library','author'),(2,'library','book'),(10,'library','bookprofile'),(3,'library','userprofile'),(9,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-09-01 11:49:57.980098'),(2,'auth','0001_initial','2020-09-01 11:50:00.430471'),(3,'admin','0001_initial','2020-09-01 11:50:16.850527'),(4,'admin','0002_logentry_remove_auto_add','2020-09-01 11:50:21.918780'),(5,'admin','0003_logentry_add_action_flag_choices','2020-09-01 11:50:21.979549'),(6,'contenttypes','0002_remove_content_type_name','2020-09-01 11:50:25.190791'),(7,'auth','0002_alter_permission_name_max_length','2020-09-01 11:50:27.903908'),(8,'auth','0003_alter_user_email_max_length','2020-09-01 11:50:28.006393'),(9,'auth','0004_alter_user_username_opts','2020-09-01 11:50:28.060499'),(10,'auth','0005_alter_user_last_login_null','2020-09-01 11:50:29.675074'),(11,'auth','0006_require_contenttypes_0002','2020-09-01 11:50:29.726956'),(12,'auth','0007_alter_validators_add_error_messages','2020-09-01 11:50:29.793196'),(13,'auth','0008_alter_user_username_max_length','2020-09-01 11:50:32.630278'),(14,'auth','0009_alter_user_last_name_max_length','2020-09-01 11:50:34.189516'),(15,'auth','0010_alter_group_name_max_length','2020-09-01 11:50:34.290566'),(16,'auth','0011_update_proxy_permissions','2020-09-01 11:50:34.341748'),(17,'library','0001_initial','2020-09-01 11:50:37.100952'),(18,'sessions','0001_initial','2020-09-01 11:50:46.514070'),(19,'library','0002_auto_20200901_1621','2020-09-01 11:51:17.469235'),(20,'library','0003_auto_20200902_1426','2020-09-02 09:56:36.591880'),(21,'library','0004_auto_20200902_1542','2020-09-02 11:12:17.651645'),(22,'library','0005_auto_20200904_0952','2020-09-04 05:22:54.906636'),(23,'library','0006_auto_20200904_1028','2020-09-04 05:58:10.470204'),(24,'library','0007_auto_20200904_1033','2020-09-04 06:03:15.170203'),(25,'library','0008_book_book_slug','2020-09-05 08:36:43.384803'),(26,'library','0009_auto_20200905_1307','2020-09-05 08:37:07.529056'),(27,'library','0010_auto_20200906_1046','2020-09-06 06:17:02.385741'),(28,'library','0011_auto_20200906_1056','2020-09-06 06:26:03.040815'),(29,'library','0012_auto_20200906_1227','2020-09-06 08:00:27.146701'),(30,'library','0013_auto_20200906_1229','2020-09-06 08:00:27.471128'),(31,'library','0014_userprofile_profile_status','2020-09-15 08:18:37.498933'),(32,'library','0015_auto_20200915_1251','2020-09-15 08:21:56.924544'),(33,'library','0016_auto_20200915_1442','2020-09-15 10:12:41.437207');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('4bl58evsqc0euf50cz40l1a7nfl51v6o','MDY5M2FjZTZhYTYwM2I2MTM3ZWM4YjIzYTQzMTUwYWI1ZDNhNjQ0Nzp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMzE0YmZlYWI2NzYwMTQ4YWQyOGM5NTNlZjM2NDM5OTFlZWZjODI2In0=','2020-09-29 10:55:52.145373'),('4yxdj9pdk14ahsiiqdl2bmvrtt1d0u0s','OTBiZmJlYTdlMTVlOTJmM2U3YTE4NWRhMGE4MDI2ZTZjNjgyNDNmNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNzgxNjBhOTllMTM0MWIwODkxMGI5ZGRmNDdlYTI0YzZjN2Y2ZDI3In0=','2020-09-17 06:11:07.157790'),('e2qvwet14w2jye1ioi8elinri98ejyux','MDY5M2FjZTZhYTYwM2I2MTM3ZWM4YjIzYTQzMTUwYWI1ZDNhNjQ0Nzp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMzE0YmZlYWI2NzYwMTQ4YWQyOGM5NTNlZjM2NDM5OTFlZWZjODI2In0=','2020-09-29 11:29:27.880321'),('ezsq1oklpcpehh9j358dveuhfi5dfiw5','OTBiZmJlYTdlMTVlOTJmM2U3YTE4NWRhMGE4MDI2ZTZjNjgyNDNmNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIxNzgxNjBhOTllMTM0MWIwODkxMGI5ZGRmNDdlYTI0YzZjN2Y2ZDI3In0=','2020-09-17 07:19:22.009093'),('j4r2yv23chtns5t3q1dej592xtyv9bsv','M2NhYjA4YjlhZmFlMzVkYjcxMmU5OTM0Y2I3ZDU3YWY0MTQwNDIyMTp7Il9hdXRoX3VzZXJfaWQiOiI1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4OTU3ODA1MGZkMmNjMTBkNWJmOGZkODA4MGI1YzI2ODI4YjU2M2ZkIn0=','2020-09-28 10:47:22.985770');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_author`
--

DROP TABLE IF EXISTS `library_author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(50) NOT NULL,
  `author_major_field` varchar(50) NOT NULL,
  `author_description` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_author`
--

LOCK TABLES `library_author` WRITE;
/*!40000 ALTER TABLE `library_author` DISABLE KEYS */;
INSERT INTO `library_author` VALUES (1,'mr','CE','age = 19'),(2,'J.K. Rowling','-','her name when her first Harry Potter book was published was simply Joanne Rowling.'),(3,'Stephenie Meyer','-','Stephenie Meyer is the author of the bestselling Twilight series,'),(4,'Arthur Golden','art history','Arthur Golden was born in Chattanooga, Tennessee, and was educated at Harvard College, where he received a degree in art history, specializing in Japanese art.'),(5,'Margaret Mitchell','-','Margaret Munnerlyn Mitchell, popularly known as Margaret Mitchell, was an American author, who won the Pulitzer Prize in 1937 for her novel, Gone with the Wind.'),(6,'George Orwell','-','Eric Arthur Blair (25 June 1903 – 21 January 1950), known by his pen name George Orwell, was an English novelist, essayist, journalist and critic.');
/*!40000 ALTER TABLE `library_author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_book`
--

DROP TABLE IF EXISTS `library_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(50) NOT NULL,
  `book_genre` varchar(30) NOT NULL,
  `book_published_date` date NOT NULL,
  `book_score_count` int(11) NOT NULL,
  `book_score_sum` int(11) NOT NULL,
  `book_description` longtext NOT NULL,
  `book_author_id` int(11) NOT NULL,
  `book_slug` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `library_book_book_author_id_16b96a6a_fk_library_author_id` (`book_author_id`),
  KEY `library_book_book_slug_219dece2` (`book_slug`),
  CONSTRAINT `library_book_book_author_id_16b96a6a_fk_library_author_id` FOREIGN KEY (`book_author_id`) REFERENCES `library_author` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_book`
--

LOCK TABLES `library_book` WRITE;
/*!40000 ALTER TABLE `library_book` DISABLE KEYS */;
INSERT INTO `library_book` VALUES (1,'Rick & Morty','sci-fi','2020-09-02',0,0,'A cool book!',1,'rick-and-morty'),(2,'Harry Potter and the Order of the Phoenix','fantasy','2015-06-21',0,0,'There is a door at the end of a silent corridor. And it’s haunting Harry Pottter’s dreams. Why else would he be waking in the middle of the night, screaming in terror?',2,'harry-potter-and-the-order-of-the-phoenix'),(3,'Twilight','fantasy','2014-10-05',0,0,'About three things I was absolutely positive.\r\n\r\nFirst, Edward was a vampire.\r\n\r\nSecond, there was a part of him—and I didn\'t know how dominant that part might be—that thirsted for my blood.\r\n\r\nAnd third, I was unconditionally and irrevocably in love with him.',3,'twilight'),(4,'Harry Potter and the Cursed Child','fantasy','2016-07-31',0,0,'Harry Potter and the Cursed Child is the eighth story in the Harry Potter series and the first official Harry Potter story to be presented on stage.',2,'harry-potter-and-the-cursed-child'),(5,'Harry Potter and the Half-Blood Prince','fantasy','2014-07-16',0,0,'The war against Voldemort is not going well; even Muggle governments are noticing. Ron scans the obituary pages of the Daily Prophet, looking for familiar names. Dumbledore is absent from Hogwarts for long stretches of time, and the Order of the Phoenix has already suffered losses.',2,'harry-potter-and-the-half-blood-prince'),(6,'Memoirs of a Geisha','fantasy','1997-09-23',0,0,'A literary sensation and runaway bestseller, this brilliant debut novel presents with seamless authenticity and exquisite lyricism the true confessions of one of Japan\'s most celebrated geisha.',4,'memoirs-of-a-geisha'),(7,'Gone with the Wind','fantasy','1999-04-01',0,0,'Scarlett O\'Hara, the beautiful, spoiled daughter of a well-to-do Georgia plantation owner, must use every means at her disposal to claw her way out of the poverty she finds herself in after Sherman\'s March to the Sea.',5,'gone-with-the-wind'),(8,'1984','sci-fi','1949-06-08',0,0,'Nineteen Eighty-Four: A Novel, often published as 1984, is a dystopian novel by English novelist George Orwell. It was published on 8 June 1949 by Secker & Warburg as Orwell\'s ninth and final book completed in his lifetime.',6,'1984');
/*!40000 ALTER TABLE `library_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_bookprofile`
--

DROP TABLE IF EXISTS `library_bookprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_bookprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookprofile_expire` datetime(6) NOT NULL,
  `bookprofile_book_id` int(11) NOT NULL,
  `bookprofile_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `library_bookprofile_bookprofile_user_id_a071fd0a` (`bookprofile_user_id`),
  KEY `library_bookprofile_bookprofile_book_id_88c45d90` (`bookprofile_book_id`),
  CONSTRAINT `library_bookprofile_bookprofile_user_id_a071fd0a_fk_library_u` FOREIGN KEY (`bookprofile_user_id`) REFERENCES `library_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_bookprofile`
--

LOCK TABLES `library_bookprofile` WRITE;
/*!40000 ALTER TABLE `library_bookprofile` DISABLE KEYS */;
INSERT INTO `library_bookprofile` VALUES (14,'2020-09-14 19:30:00.000000',1,4),(25,'2020-09-15 19:30:00.000000',1,2);
/*!40000 ALTER TABLE `library_bookprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `library_userprofile`
--

DROP TABLE IF EXISTS `library_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `library_userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_user_id` int(11) NOT NULL,
  `profile_status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile_user_id` (`profile_user_id`),
  CONSTRAINT `library_userprofile_profile_user_id_10ac2771_fk_auth_user_id` FOREIGN KEY (`profile_user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `library_userprofile`
--

LOCK TABLES `library_userprofile` WRITE;
/*!40000 ALTER TABLE `library_userprofile` DISABLE KEYS */;
INSERT INTO `library_userprofile` VALUES (1,1,'offline'),(2,3,'online'),(3,4,'offline'),(4,5,'offline');
/*!40000 ALTER TABLE `library_userprofile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-20 12:44:55
