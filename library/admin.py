from django.contrib import admin

from .models import Book, UserProfile, Author, BookProfile

# Register your models here.
class BookAdmin(admin.ModelAdmin):
    list_display = ('book_name', 'book_genre', 'book_published_date',
                    'book_score_count', 'book_score_sum',
                    'book_author')

    list_filter = ('book_published_date', 'book_genre', 'book_author')

    search_fields = ('book_name', 'book_description')
    
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('profile_user',)

class AuthorAdmin(admin.ModelAdmin):
    list_display = ('author_name',)

class BookProfileAdmin(admin.ModelAdmin):
    list_display = ('bookprofile_book', 'bookprofile_user', 'bookprofile_expire')

admin.site.register(Book, BookAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(BookProfile, BookProfileAdmin)
