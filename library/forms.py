from django import forms
from django.contrib.auth.models import User
from .models import BookProfile
from django.contrib.admin import widgets
import datetime

class BorrowForm(forms.Form):
    expire_date = forms.DateField(widget=forms.SelectDateWidget(),
                                initial=datetime.date.today() + 
                                datetime.timedelta(days=1))

    def clean_expire_date(self):
        expire_date = self.cleaned_data['expire_date']
        if expire_date < datetime.date.today():
            raise forms.ValidationError("The expire date cannot be in the past!!")
        return expire_date