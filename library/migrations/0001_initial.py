# Generated by Django 3.0.9 on 2020-09-01 11:49

import datetime
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author_name', models.CharField(max_length=50)),
                ('author_major_field', models.CharField(max_length=50)),
                ('author_description', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('book_name', models.CharField(max_length=50)),
                ('book_genre', models.CharField(choices=[('fantay', 'Fantasy'), ('sci-Fi', 'Sci-Fi'), ('mystery', 'Mystery'), ('thriller', 'Thriller'), ('romance', 'Romance'), ('western', 'Western'), ('dystopian', 'Dystopian'), ('contemporary', 'Contemporary')], max_length=30)),
                ('book_published_date', models.DateField()),
                ('book_score_count', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)])),
                ('book_score_sum', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)])),
                ('book_description', models.TextField()),
                ('book_author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='library.Author')),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('profile_registration_date', models.DateTimeField(default=datetime.datetime(2020, 9, 1, 11, 49, 45, 609259, tzinfo=utc))),
                ('profile_books_borrowed', models.ManyToManyField(to='library.Book')),
                ('profile_user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
