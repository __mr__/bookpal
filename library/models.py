from django.db import models
from django.conf import global_settings
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator

class Author(models.Model):
    
    author_name = models.CharField(max_length=50)

    author_major_field = models.CharField(max_length=50)

    author_description = models.TextField()

    def __str__(self):
        return self.author_name

class Book(models.Model):
    book_name = models.CharField(max_length=50)

    book_slug = models.SlugField(max_length=60)
    
    GENRES_LIST = (
                    ("fantasy", "Fantasy"), 
                    ("sci-fi", 'Sci-Fi'),
                    ('mystery', 'Mystery'),
                    ('thriller', 'Thriller'),
                    ('romance', 'Romance'),
                    ('western','Western'),
                    ('dystopian','Dystopian'),
                    ('contemporary', 'Contemporary')
                )
    book_genre = models.CharField(max_length=30, choices=GENRES_LIST)
    
    book_published_date = models.DateField()

    book_score_count = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    book_score_sum = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    def get_book_score(self):
        try:
            return self.book_score_sum / self.book_score_count
        except:
            return 0

    book_description = models.TextField()

    book_author = models.ForeignKey(Author, on_delete=models.CASCADE,
                                    related_name = "author_books")

    #book_pic = 


    def __str__(self):
        return self.book_name

class UserProfile(models.Model):
    profile_user = models.OneToOneField(global_settings.AUTH_USER_MODEL,
                                    on_delete=models.CASCADE,
                                    related_name="user_profile")

    profile_status = models.CharField(max_length=10, choices=(('online', 'Online'), ('offline', 'Offline')))

    def __str__(self):
        return self.profile_user.username

class BookProfile(models.Model):

    bookprofile_expire = models.DateTimeField()

    bookprofile_user = models.ForeignKey(UserProfile, on_delete=models.CASCADE,
                                            related_name="profile_books_borrowed")

    bookprofile_book = models.ForeignKey(Book, on_delete=models.CASCADE)

    def __str__(self):
        return "{0} borrowed by:{1}, until {2}".format(self.bookprofile_book,
                                                    self.bookprofile_user,
                                                    self.bookprofile_expire)
