from django.urls import path

from . import views

app_name = "library"

urlpatterns = [
        path('', views.index, name = 'index'),
        path('library/', views.redirect_to_index),
        path('library/<slug:book_slug>', views.book_info, name='book'),
        path('library/category/<cate>', views.category, 
                                            name='category'),
]
