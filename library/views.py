from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse

from .models import Book, BookProfile
from .forms import BorrowForm


# Create your views here.
def index(req):
    latest_books = Book.objects.order_by('-book_published_date')[:3]
    categories = []
    for i, j in Book.GENRES_LIST:
        categories.append(i)
    return render(req, 'library/index.html', { 'latest_books':latest_books,
                                            'categories':categories })

def category(req, cate):
    ls = [i[0] for i in Book.GENRES_LIST]
    if cate in ls:
        books = Book.objects.filter(book_genre=cate)
        return render(req, 'library/category.html', { 'books': books, 'cate':cate })
    else:
        raise Http404

def book_info(req, book_slug):
    
    book = get_object_or_404(Book, book_slug=book_slug)
    
    try:
        book_profile = BookProfile.objects.get(bookprofile_user=req.user.user_profile, bookprofile_book=book)
    except:
        book_profile = None
    
    try:
        score = book_score_sum / book_score_count
    except:
        score = 0

    if req.method != "POST":
        borrow_form = BorrowForm()
    else:
        borrow_form = BorrowForm(data=req.POST)
        if borrow_form.is_valid():
            cd = borrow_form.cleaned_data
            new_book = BookProfile.objects.create(bookprofile_expire=cd['expire_date'], bookprofile_user=req.user.user_profile, bookprofile_book = book)
            return HttpResponseRedirect(req.path)

    return render(req, 'library/book_info.html', { 'book': book,
                                        'book_profile':book_profile,
                                        'score':score,
                                        'borrow_form':borrow_form})

def redirect_to_index(req):
    return HttpResponseRedirect(reverse('library:index'))
